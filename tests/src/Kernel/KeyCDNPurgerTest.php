<?php

namespace Drupal\Tests\keycdn\Kernel;

use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\keycdn\Plugin\Purge\Purger\KeyCDNPurger;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Prophecy\Argument;

/**
 * Class KeyCDNPurgerTest.
 *
 * @covers \Drupal\keycdn\Plugin\Purge\Purger\KeyCDNPurger
 * @package Drupal\keycdn\Tests\KeyCDNPurgerTest
 */
class KeyCDNPurgerTest extends KernelTestBase {

  public static $modules = [
    'keycdn',
    'purge'
  ];

  /**
   * @covers \Drupal\keycdn\Plugin\Purge\Purger\KeyCDNPurger::getIdealConditionsLimit
   */
  public function testGetIdealConditionsLimit() {
    $params = $this->getPurgerParams();
    // Add mock return for an environment with 3 other KeyCDN purgers.
    $plugins = [
      ['plugin_id' => 'purge_purger_keycdn'],
      ['plugin_id' => 'purge_purger_keycdn'],
      ['plugin_id' => 'purge_purger_keycdn'],
      ['plugin_id' => 'other_purger'],
    ];
    $purge_plugins_config_prophecy = $this->prophesize('Drupal\Core\Config\ImmutableConfig');
    $purge_plugins_config_prophecy->get('purgers')->willReturn($plugins);
    $params['factory_prophecy']->get('purge.plugins')->willReturn($purge_plugins_config_prophecy->reveal());

    // Instantiate and test the purger.
    $purger = $this->getMockPurger($params);
    self::assertEquals(192, $purger->getIdealConditionsLimit());
  }

  /**
   * @covers \Drupal\keycdn\Plugin\Purge\Purger\KeyCDNPurger::invalidateItems
   */
  public function testInvalidateItemsSuccess() {
    $id = 'test_purger';
    $params = $this->getPurgerParams($id);
    // Set a dummy zone config.
    $dummy_config = $this->prophesize('Drupal\Core\Config\ImmutableConfig');
    $dummy_config->get('zone')->willReturn('test_zone');
    $dummy_config->get('api_key')->willReturn('12345');
    $dummy_config->reveal();
    $params['factory_prophecy']->get("keycdn.settings.{$id}")->willReturn($dummy_config);
    $purger = $this->getMockPurger($params);
    // Add a logger.
    $logger = $this->prophesize('Psr\Log\LoggerInterface');
    $logger->debug('KeyCDN purge successful. Uri: %uri, Params: %params', Argument::cetera())->shouldBeCalled();
    $purger->setLogger($logger->reveal());
    // Invalidate a tag.
    $invalidation = $this->prophesize('Drupal\purge\Plugin\Purge\Invalidation\TagInvalidation');
    $invalidation->setState(Argument::any())->shouldBeCalledTimes(2);
    $invalidation->setState(InvalidationInterface::SUCCEEDED)->shouldBeCalledTimes(1);
    $invalidation->setState(InvalidationInterface::PROCESSING)->shouldBeCalledTimes(1);
    $invalidation->setState(Argument::not(InvalidationInterface::FAILED));
    $invalidation->getExpression()->willReturn('tag1');
    $purger->invalidateTags([$invalidation->reveal()]);

    // Invalidate a Url.
    $invalidation = $this->prophesize('Drupal\purge\Plugin\Purge\Invalidation\UrlInvalidation');
    $invalidation->setState(Argument::any())->shouldBeCalledTimes(2);
    $invalidation->setState(InvalidationInterface::SUCCEEDED)->shouldBeCalledTimes(1);
    $invalidation->setState(InvalidationInterface::PROCESSING)->shouldBeCalledTimes(1);
    $invalidation->setState(Argument::not(InvalidationInterface::FAILED));
    $invalidation->getUrl()->willReturn(Url::fromUri('http://example.com/path', ['absolute' => TRUE]));
    $purger->invalidateUrls([$invalidation->reveal()]);

  }

  /**
   * Helper function to get prophecied purger parameters.
   *
   * @param string $id
   *   The Id of the purger instance you want to instantiate.
   *
   * @return array
   *   The parameters for instantiating KeyCDNPurger.
   */
  private function getPurgerParams($id = 'test_purger') {

    // Get an instance with which to test.
    $configuration = ['id' => 'test_purger'];
    $plugin_id = 'purge_purger_keycdn';
    // The httpclient mock should always return 200 success.
    $response = $this->prophesize('Psr\Http\Message\ResponseInterface');
    $response->getStatusCode()->willReturn(200);
    $response->getBody()->willReturn(json_encode(['status' => 'success', 'body' => '']));
    $http_client_prophecy = $this->prophesize('GuzzleHttp\Client');
    $http_client_prophecy->request(Argument::cetera())->willReturn($response->reveal());
    $factory_prophecy = $this->prophesize('Drupal\Core\Config\ConfigFactory');
    $plugin_definition = [];
    $dummy_config = $this->prophesize('Drupal\Core\Config\ImmutableConfig');
    $factory_prophecy->get("keycdn.settings.{$id}")->willReturn($dummy_config->reveal());
    return [
      'configuration' => $configuration,
      'plugin_id' => $plugin_id,
      'plugin_definition' => $plugin_definition,
      'http_client_prophecy' => $http_client_prophecy,
      'factory_prophecy' => $factory_prophecy,
    ];
  }

  /**
   * Instantiates and returns a mocked KeyCDN Purger.
   *
   * @param array $params
   *   Parameters for the plugin, as returned by $this->getPurgerParams().
   *
   * @return \Drupal\keycdn\Plugin\Purge\Purger\KeyCDNPurger
   *   The instantiated KeyCDN Purger.
   */
  private function getMockPurger(array $params) {
    $configuration = $params['configuration'];
    $plugin_id = $params['plugin_id'];
    $plugin_definition = $params['plugin_definition'];
    $http_client = $params['http_client_prophecy']->reveal();
    $factory = $params['factory_prophecy']->reveal();
    return new KeyCDNPurger($configuration, $plugin_id, $plugin_definition, $http_client, $factory);
  }
}
